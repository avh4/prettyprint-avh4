{-# OPTIONS_GHC -Wno-deprecations #-}

module Text.PrettyPrint.Avh4.BlockSpec where

import Data.ByteString.Lazy.Char8 (ByteString)
import Test.Hspec
import Test.Hspec.QuickCheck
import TestPrelude
import Text.PrettyPrint.Avh4.Block

spec :: Spec
spec =
  describe "Block" $ do
    describe "Line" $ do
      describe "Semigroup instance" $ do
        prop "associativity" $
          \(x :: Line) y z ->
            (x <> (y <> z))
              `shouldRenderAs` ((x <> y) <> z)

    it "trims trailing whitespace" $
      line (space <> space <> space) `shouldFormatAs` [""]

    it "formats a trivial line" $ do
      line (string7 "word")
        `shouldFormatAs` ["word"]

    describe "stack" $ do
      prop "stack . pure == id" $
        \x ->
          (stack . pure) x `shouldRenderAs` x

    describe "rowOrStack" $ do
      prop "rowOrStack . pure == id" $
        \x ->
          (rowOrStack undefined . pure) x `shouldRenderAs` x

      it "joins single lines" $
        rowOrStack
          (Just $ char7 ':')
          [line $ string7 "a", line $ string7 "b"]
          `shouldFormatAs` ["a:b"]

      it "splits when the first is multiline" $
        rowOrStack
          (Just $ char7 ':')
          [mustBreak $ string7 "a", line $ string7 "b"]
          `shouldFormatAs` [ "a",
                             "b"
                           ]

      it "combines when only second is multiline" $
        rowOrStack
          (Just $ char7 ':')
          [line $ string7 "a", mustBreak $ string7 "b"]
          `shouldFormatAs` ["a:b"]

      it "can result in single line" $
        rowOrStack
          (Just $ char7 '-')
          [ rowOrStack
              (Just $ char7 '+')
              [ line $ string7 "a",
                line $ string7 "b"
              ],
            line $ string7 "c"
          ]
          `shouldFormatAs` ["a+b-c"]

      it "retains multiline" $
        rowOrStack
          (Just $ char7 '-')
          [ rowOrStack
              (Just $ char7 '+')
              [ mustBreak $ string7 "a",
                line $ string7 "b"
              ],
            line $ string7 "c"
          ]
          `shouldFormatAs` [ "a",
                             "b",
                             "c"
                           ]

      it "retains mustBreak" $
        rowOrStack
          (Just $ char7 '-')
          [ rowOrStack
              (Just $ char7 '+')
              [ line $ string7 "a",
                mustBreak $ string7 "b"
              ],
            line $ string7 "c"
          ]
          `shouldFormatAs` ["a+b", "c"]

    describe "rowOrIndent" $ do
      prop "rowOrIndent . pure == id" $
        \x ->
          (rowOrIndent undefined . pure) x `shouldRenderAs` x

      it "joins single lines" $
        rowOrIndent
          (Just $ char7 ':')
          [line $ string7 "a", line $ string7 "b"]
          `shouldFormatAs` ["a:b"]

      it "splits when the first is multiline" $
        rowOrIndent
          (Just $ char7 ':')
          [mustBreak $ string7 "a", line $ string7 "b"]
          `shouldFormatAs` [ "a",
                             "    b"
                           ]

      it "retains mustBreak" $
        rowOrStack
          (Just $ char7 '-')
          [ rowOrIndent
              (Just $ char7 '+')
              [ line $ string7 "a",
                mustBreak $ string7 "b"
              ],
            line $ string7 "c"
          ]
          `shouldFormatAs` ["a+b", "c"]

    describe "prefix" $ do
      it "prefixes a single line" $
        prefix 3 (string7 "::" <> space) (line $ string7 "A")
          `shouldFormatAs` [":: A"]
      it "pads remaining lines" $
        prefix 3 (string7 "::" <> space) (stack $ fmap (line . string7) ["A", "B"])
          `shouldFormatAs` [ ":: A",
                             "   B"
                           ]
      it "does not produce trailing whitespace when prefixing a blank line" $ do
        prefix 2 (char7 '-' <> space) (stack [blankLine, line $ string7 "a"])
          `shouldFormatAs` [ "-",
                             "  a"
                           ]

    describe "prefixOrIndent" $ do
      it "prefixes a single line" $
        prefixOrIndent
          Nothing
          (string7 ".")
          (line $ string7 "a")
          `shouldFormatAs` [".a"]

      it "uses the joiner" $
        prefixOrIndent
          (Just $ char7 ':')
          (string7 "a")
          (line $ string7 "b")
          `shouldFormatAs` ["a:b"]

      it "indents multiline" $
        prefixOrIndent
          undefined
          (string7 "a")
          (stack [line $ string7 "b", line $ string7 "c"])
          `shouldFormatAs` [ "a",
                             "    b",
                             "    c"
                           ]

      prop "is the same as rowOrIndent" $ do
        \j b ->
          let p = string7 "aaa"
           in prefixOrIndent j p b
                `shouldRenderAs` rowOrIndent j [line p, b]

    describe "addSuffix" $ do
      it "suffixes a single line" $
        addSuffix (string7 "...") (line $ string7 "A")
          `shouldFormatAs` ["A..."]
      it "suffixes the last line of a multiline block" $
        addSuffix (string7 "...") (stack $ fmap (line . string7) ["A", "B"])
          `shouldFormatAs` [ "A",
                             "B..."
                           ]

    describe "joinMustBreak" $ do
      it "joins single lines" $
        joinMustBreak
          (line $ string7 "a")
          (line $ string7 "b")
          `shouldFormatAs` ["a b"]

      it "splits when the first is multiline" $
        joinMustBreak
          (mustBreak $ string7 "a")
          (line $ string7 "b")
          `shouldFormatAs` [ "a",
                             "b"
                           ]

      it "combines when only second is multiline" $
        joinMustBreak
          (line $ string7 "a")
          (mustBreak $ string7 "b")
          `shouldFormatAs` ["a b"]

      it "retains mustBreak" $
        rowOrStack
          (Just $ char7 '-')
          [ joinMustBreak
              (line $ string7 "a")
              (mustBreak $ string7 "b"),
            line $ string7 "c"
          ]
          `shouldFormatAs` ["a b", "c"]

shouldFormatAs :: Block -> [ByteString] -> IO ()
shouldFormatAs =
  shouldRenderAs
