{-# OPTIONS_GHC -Wno-orphans #-}

module TestPrelude where

import qualified Data.ByteString.Builder as B
import qualified Data.ByteString.Lazy as Lazy
import qualified Data.ByteString.Lazy.Char8 as Lazy
import Test.Hspec
import Test.QuickCheck
import Text.PrettyPrint.Avh4.Block (Block)
import qualified Text.PrettyPrint.Avh4.Block as Block

class Renderable a where
  render' :: a -> Lazy.ByteString

instance Renderable Lazy.ByteString where
  render' = id

instance Renderable [Lazy.ByteString] where
  render' = Lazy.unlines

instance Renderable Block where
  render' = B.toLazyByteString . Block.render

instance Renderable Block.Line where
  render' = render' . Block.line

shouldRenderAs :: (Renderable actual, Renderable expected) => actual -> expected -> IO ()
shouldRenderAs actual expected =
  render' actual `shouldBe` render' expected

--
-- QuickCheck
--

instance Arbitrary Block.Line where
  arbitrary =
    oneof
      [ Block.stringUtf8 <$> listOf1 (oneof $ fmap pure "ABC"),
        pure Block.space,
        (<>) <$> arbitrary <*> arbitrary
      ]

instance Arbitrary Block where
  arbitrary =
    oneof
      [ Block.line <$> arbitrary
      ]
