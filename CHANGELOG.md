# 0.1.1.0 (2023-04-14)

API additions:

- Added `spaceSeparatedOr*` convenience functions

Bug fixes:

- `rowOr*` functions will now correctly join groups of single lines ending with a `mustBreak` line


# 0.1.0.0 (2023-03-29)

- Initial release

