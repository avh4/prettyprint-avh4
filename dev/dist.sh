#!/bin/bash
set -euxo pipefail

hpack
cabal check
dev/ci.sh
cabal haddock --haddock-for-hackage --enable-doc
cabal sdist
