#!/bin/bash
set -euxo pipefail

dev/build.sh
simple-http-server --index dist-newstyle/build/x86_64-linux/ghc-9.2.5/prettyprint-avh4-0.1.1.0/doc/html/prettyprint-avh4
