#!/bin/bash
set -euxo pipefail

dev/build.sh
nix-build -A ghc925.prettyprint-avh4
nix-build -A ghc944.prettyprint-avh4
