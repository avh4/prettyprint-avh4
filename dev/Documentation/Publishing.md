# How to publish a release

## Preparation

1. Choose the new version number according to the [Haskell PVP Specification](https://pvp.haskell.org/).
1. Start a new branch from `origin/main` named `release/{{new version}}`
    - `git fetch origin`
    - `git switch -c release/{{new version}} origin/main`
1. Update `CHANGELOG.md`:
    - set the correct version number and release date
    - review it for accuracy and completeness
1. Update the version number in `package.yaml`, and then run `dev/build.sh` to ensure that generated files get updated.
1. Check `git grep {{old version}}` for any other files and scripts that need to be updated.
1. Commit the changes with the commit message "Prepare {{new version}} release"
1. Run `dev/ci.sh` and ensure that it passes.
1. Push the `release/{{new version}}` branch and create a PR.
1. Create a signed tag for the new version:
    - `git tag -s {{new version}} -m {{new version}}`
1. Push the tag:
    - `git push origin {{new version}}`


## Publishing

1. Ensure that you have the exact tag checked out and that `git status` is clean.
1. Run `dev/dist.sh` to build the release artifacts, and ensure it completes successfully.
1. Go to <https://hackage.haskell.org/packages/candidates/upload> and upload the file `dist-newstyle/sdist/prettyprint-avh4-{{new version}}.tar.gz`
1. Review the uploaded package candidate for any problems.
1. If everything looks good, click "\[Publish\]".
1. Within 24 hours, hackage should build the documentation and email you that it was successful.


## Cleanup

1. Merge the PR for the `release/{{new version}}` branch, and delete the branch.
