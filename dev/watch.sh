#!/bin/bash
set -euxo pipefail

hpack
ghcid --target=prettyprint-avh4-tests --run "$@"
