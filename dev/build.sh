#!/bin/bash
set -euxo pipefail

hpack
cabal build
cabal test --enable-coverage
cabal haddock
