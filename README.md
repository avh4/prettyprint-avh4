This package provides an API for structured text layout,
used by [elm-format](https://github.com/avh4/elm-format) and [gren](https://gren-lang.org/)
for source code formatting.

Compared to other pretty-printing approaches, this library has the following characteristics:

  - Fitting to a maximum column width is not supported (and is considered an anti-feature)
  - The API has a design goal of making it easy to write text formatters for custom syntax trees
  - The API is meant primarily for use in bottom-up folds
  - `ByteString.Builder` is used as the underlying primitive type to optimize memory allocation (especially when writing to an output stream)


## Future improvements

Contributions for the following are welcome:

  - Add benchmarks and/or improve performance
  - Improve documentation
  - Provide new use-cases for writing syntax tree formatters, and improve the API design

If there is some other existing Haskell library that fulfills the goals of this project,
I'd be interested in deprecating this project in favor of the other.

